class Customer < ApplicationRecord
  has_one :address, :dependent => :destroy
  has_many :credit_cards
  has_many :reservations
  validates :last_name, :first_name, :has_good_credit, :paid, presence: true
end
