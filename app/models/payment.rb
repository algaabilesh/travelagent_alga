class Payment < ApplicationRecord
  belongs_to :customer
  belongs_to :reservation
  validates :amount, :card_number, presence: true
end
